# !/bin/bash
#Programa de monitorizacion atraves de apache2
#Enseña por el navegador los discos 
echo "<h1> Discos </h1>" > /var/www/html/informe.html
echo "<p> <hr><pre> `df -h ` <hr></p> " >> /var/www/html/informe.html
#Enseña por navegador la memoria ocupada y libre
echo "<h1> Memoria RAM </h1>" >> /var/www/html/informe.html
echo "<p> <hr> <pre> `free -h ` <hr> </p> " >> /var/www/html/informe.html
#Enseña por navegador los procesos activos de la maquina
echo "<h1> Procesos activos  </h1>" >> /var/www/html/informe.html
echo "<p> <hr> <pre>`sudo service --status-all | grep +` <hr> </p> " >> /var/www/html/informe.html
#Enseña por navegador el tipo de tarjeta de red de la maquina
echo "<h1> Tarjeta de red </h1>" >> /var/www/html/informe.html
echo "<p> <pre> <hr> `lspci | grep Wireless`<hr> </p>" >> /var/www/html/informe.html
#Enseña por navegador los puertos USB de la maquina 
echo "<h1> Puertos USB</h1>" >> /var/www/html/informe.html
echo "<p> <pre> <hr> `lsusb` <hr> </p>" >> /var/www/html/informe.html