# !bin/bash/
# MENU DE INSTALACION DE VARIAS APLICACIONES 
pausa() {
read -p  "Pulsa [Intro] para continuar.." fackEnterKey
}
uno(){
     sudo apt-get install chromium -y  
     echo "Listo!" 
     pausa   
}
dos(){
     cd /tmp/
     sudo wget https://go.microsoft.com/fwlink/?LinkID=760868
     mv index.html\?LinkID\=760868 code.deb
     sudo dpkg -i code.deb
     echo "Listo!"
     pausa
}
menu(){
     clear
     echo "===Menu de Instalacion==="
     echo "1.Instalar Chromium"
     echo "2.Instalar Visual Studio Code"
     echo "3.Salir"
     echo "========================="

}
opciones(){
    local opciones
    read -p "Elige una opcion [1 - 3]" opciones
    case $opciones in 
    1)uno ;;
    2)dos ;;
    3)exit 0;;
    *) echo -e {$RED}"No es una opcion"{$STD} && sleep 2
    esac 
}
trap '' SIGINT SIGQUIT SIGTSTP 
while true 
do 
menu
opciones 
done
