<!DOCTYPE html>
<html lang="en">
<head>
  <title>PHP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="bg-success">
<div class="container">  
  <h1 class="text-primary">Bienvenido</h1>
  </div>
<div class="container">
    <h1 style="color:rgb(0,255,20);"> <abbr title="Procesador de HyperTexto"> PHP5</abbr> </h1>
    </div>
    <div class="container"> 
    <h2 class="text-danger">¿Que es?</h2>
    <p class="text-primary"> <pre> <strong>
Lenguaje de la arquitectura LAMP para el desarrollo web de contenido dinámico.
Puede introducirse directamente en html sin llamar algun archivo externo.
Incluye una interfaz de linea de comandos que puede ser usada en aplicaciones web.
Añade comandos de bash para el propio servidor web.
<strong>
</div>
<div class="container">
    <h2 class="text-danger">Sintaxis</h2>
    <p class="text-primary"> <pre> <strong>
En php cada codigo que escribas se empieza con "<+?php" y acaba con "?>"
y los comandos que ponganmos dentro del codigo acaba en ";".
Los comentarios se escriben empezando con "//"  y "#" para una sola linea 
y para mas de 1 linea se escriben empezando con "/*" y acabados en "*/".
Se pueden asignar variables y funciones 
Hay funciones y se representan y llaman como 'function "nombre de la funcion"(parametros) {comandos de php}'.
Se pueden abrir archivos 

<strong>
</div>
<div class="container">
  <h1 class="text-danger">Funciones</h1>
<div>
  <p class="text-primary"> <pre> <strong>
Hay muchas funciones utiles en PHP :
echo --> enseña por html todo lo que escribas.
shell_exec('cmd') --> ejecuta comandos de bash en la maquina del servidor.
echo shell_exec('cmd') --> hace lo mismo que el shell_exec solo que lo escribe en html. 
date(DATE_RFC2822) --> enseña la fecha y hora. 
return --> devuelve un resultado de algunas variables (normalmente se utiliza para enviar mensajes o hacer calculos.)
for --> ejecuta un bloque de comandos un numero de veces especifico.
while --> ejecuta un bloque de comandos si la condicion es cierta .
switch --> se usa para hacer un tipo de acciones basadas en las condiciones de la variable.
arrays --> almacena multiples tipos de variables en una sola.
SuperGlobals --> variables predefinidas por PHP
<strong>
  </p>
</div>

<?php 
function ruta(){
echo shell_exec('sudo ifconfig wlan0 | grep "inet" | cut -d: -f2 | cut -d " " -f1');
}
?>
<div class="btn-group btn-group-justified">
 <a href="<?php echo ruta() ?>/PHP2.php" class="btn btn-primary">Siguiente</a>
 <a href="http://php.net/manual/es/index.php" class="btn btn-primary">Manual PHP</a>
</div>
</body>
</html>
