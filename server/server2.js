class App{
    constructor(){}
    static lista(req,res){
        res.header("Access-Control-Allow-Origin", "*");
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').collection('productos').find().toArray((err, results) => res.send(results)) 
    })
    }
    static inserta(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').collection('productos').insertOne(req.body)
    })
    }
    static actualiza(req,res){
        MongoClient.connect(url, function(err, client) { 
            let id = req.body._id; delete req.body._id
            assert.equal(null, err);
            client.db('clase').collection('productos').update({_id: new mongo.ObjectId(id)}, req.body , function(err, res) {
                if (err) throw err;
                console.log("1 documento actualizado");
    
              })
          
    }) 
    }
    static borrar(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            let id = req.body._id; delete req.body._id;
            const db = client.db('clase');
            client.db('clase').collection('productos').deleteOne( {_id: new mongo.ObjectID(id)},
            (err, data) => { 
            res.send(data)
        }) 
        })
    }
    static dbs(req,res){
        MongoClient.connect(url, function(err, client) {
            const adminDb = client.db().admin();
            adminDb.listDatabases(function(err, dbs) {     
              res.send(dbs.databases)
            });    
          });
    }
    static collection(req,res){
        MongoClient.connect(url, function(err, client) {    
            client.db(req.query.db).listCollections().toArray( (e,d) =>   res.send(d)  );
            });
    }
    static cols(req,res){
        MongoClient.connect(url, function(err, client) {    
            client.db('clase').collection(req.query.collection).find().toArray( (e,d) =>   res.send(d) );
            });
    }
    static Delcl(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            let id = req.body._id; delete req.body._id;
            const db = client.db('clase');
            client.db('clase').collection('clientes').deleteMany({ _id: new mongo.ObjectID(id)},
            (err, data) => { 
            res.send(data)
        }) 
        })
    }
    static insertacl(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').collection('clientes').insertOne(req.body)
    })
    }
    static borrarcl(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').collection('clientes').deleteOne(req.body.id) 
        })
    }
    static updcl(req,res){
        MongoClient.connect(url, function(err, client) { 
            let id = req.body._id; delete req.body._id
            assert.equal(null, err);
            client.db('clase').collection('clientes').update({_id: new mongo.ObjectId(id)}, req.body , function(err, res) {
                if (err) throw err;
                console.log("1 documento actualizado");
    
              })
          
    }) 
    }
    static dropcol(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').collection(req.query.collection).drop(function(err, delOK) {
                if (err) throw err;
                if (delOK) console.log("Collection deleted");
              }); 
        })
    }
    static ccol(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('clase');
            client.db('clase').createCollection(req.body.name ,function(err, res) {
                if (err) throw err;
                console.log("Coleccion creada!");
              })
    })
    }
}
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongo = require('mongodb')
const MongoClient = mongo.MongoClient;
const assert = require('assert');

app.use( bodyParser.json() );
app.use( express.static('public') );
const url = process.argv[2];
app.post('/insert', App.inserta);
app.get('/adm', carga );
function carga(req, res){
     res.redirect('http://192.168.105.93:3000/MONGO/ADMINISTRADOR/admmongo.html')
}
app.get('/lista', App.lista);
app.get('/:db/:col', function(req, res){
    MongoClient.connect(url, function(err, client) { 
        useNewUrlParser: true ; 
        assert.equal(null, err);
        client.db(req.params.db).collection(req.params.col).find().toArray((err, results) => res.send(JSON.stringify(results))) 
})
});
app.post('/actualiza', App.actualiza);
/* app.post('/delete', App.borrar); */
app.get('/dbs', App.dbs)
app.get('/cols', App.collection)
app.get('/thiscol', App.cols)
app.post('/delcl', App.Delcl)
app.post('/insertcl', App.insertacl)
app.post('/updcl', App.updcl)
app.get('/drop', App.dropcol)
app.post('/ccol', App.ccol)
app.post('/delete', (request, response) => {    
    console.log(request.body)
    let id = request.body._id; delete request.body._id;    
    let db = request.body.db; delete request.body.db;
    let collection = request.body.collection; delete request.body.collection;      
    MongoClient.connect(url, function(err, client) {
      client.db(db).collection(collection).deleteOne(
        { _id: new mongo.ObjectID(id)},
        (err, data) => { 
        response.send(data);
      }); 
      client.close();
    });  
  });
  app.get('/colls', (req, res) => {
    // show collections  
    MongoClient.connect(url, function(err, client) {    
      client.db(req.query.db).listCollections().toArray( (e,d) =>  res.send(d) );
    });
  })
  app.post('/update', (request, response) => {   
    console.log('actualizando objeto: ', request.body) 
    let id = request.body._id; delete request.body._id;    
    let db = request.body.db; delete request.body.db;
    let collection = request.body.collection; delete request.body.collection;      
    MongoClient.connect(url, function(err, client) {
      client.db(db).collection(collection).update(
        { _id: new mongo.ObjectID(id)}, 
        request.body, 
        {upsert: true}, 
        (err, data) => { 
        response.send(data);
      }); 
      client.close()
    });  
  });
app.listen(3000);