const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
const conn = mysql.createConnection({ 
    host: 'localhost', 
    user: 'root', 
    password: 'a', 
    database: 'tienda'});

    app.use( bodyParser.json() );
app.use( express.static('public') );
// app.post('/guardar', inserta);
// function inserta(req, res){
//     conn.query('insert into blogs (idblogs, autor, titulo, categoria, contenido ) values (0, ?, ?, ?, ? )', [req.body.titulo, req.body.autor, req.body.categoria, req.body.contenido ],
// function(error,datos){res.send(datos)} )

// }
app.post('/guardaitem', inserta);
function inserta(req, res){
    conn.query('insert into items (id, nombre, precio, imagen ) values (0, ?, ?, ? )', [req.body.nombre, req.body.precio, req.body.imagen ],
function(error,datos){res.send(datos)} )

}
app.post('/producto', inserta);
function inserta(req, res){
    conn.query('insert into items (id, nombre, precio, imagen ) values (0, ?, ?, ? )', [req.body.nombre, req.body.precio, req.body.imagen ],
function(error,datos){res.send(datos)} )

}
app.post('/actualiza', actu);
function actu(req, res){
    conn.query('update items set nombre = ?, precio = ?, imagen = ?  where id= ?', [req.body.nombre, req.body.precio, req.body.imagen, req.body.id ],
function(error,datos){res.send(datos)} )

}
app.get('/tienda', function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    conn.query('select * from items', function(error, datos){
        if (error) throw error;
        res.send(datos);
    })
});
app.get('/delete', borrado);
function borrado(req,res){
    conn.query('delete from items where id= ?', [req.query.id],
function(error,datos){res.send(datos)}
)
}
app.get('/blogs', function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    conn.query('select * from blogs', function(error, datos){
        if (error) throw error; 
        res.send(datos)
    })
    });
    app.get('/tienda', function(req, res){
        res.header("Access-Control-Allow-Origin", "*");
        conn.query('select * from items', function(error, datos){
            if (error) throw error; 
            res.send(datos)
        });

});
// app.get('/delete', borrado);
// function borrado(req,res){
//     conn.query('delete from blogs where idblogs= ?', [req.query.idblogs],
// function(error,datos){res.send(datos)}
// )
// }


// rutas del server:

app.get('/describe/:tabla', describe );

app.get('/select/:tabla', select);

// funciones de callback
function select(req, res){
    conn.query( 
        'select * from ??', 
        [req.params.tabla], 
        function(error, results){        
            res.send(results);
    });
}



function describe(x, y){
    conn.query(
        'describe ' + x.params.tabla, 
        [], 
        function(error, results){  y.send(results)  }
    );
    
}



app.listen(3000, n => {});