import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  alumnos: any[] = []
  constructor(private http: Http){}
  ngOnInit(){ this.getDatos() }
  getDatos(){
    this.http.get('http://localhost:3000/lista')
    .subscribe( d => { console.log(d.json())})
  }

}
