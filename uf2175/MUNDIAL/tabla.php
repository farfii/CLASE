<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Mundial</title>
    <style>
  body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
  }
  h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #111;
  }
  .container {
      padding: 80px 120px;
  }
  .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
  }
  .person:hover {
      border-color: #f1f1f1;
  }
  .carousel-inner img { 
      width: 100%; 
      margin: auto;
  }
  .carousel-caption h3 {
      color: #fff !important;
  }
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
  }
  .bg-1 h3 {color: #fff;}
  .bg-1 p {font-style: italic;}
  .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
  }
  .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail p {
      margin-top: 15px;
      color: #555;
  }
  .btn {
      padding: 10px 20px;
      background-color: #333;
      color: #f1f1f1;
      border-radius: 0;
      transition: .2s;
  }
  .btn:hover, .btn:focus {
      border: 1px solid #333;
      background-color: #fff;
      color: #000;
  }
  .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-header, .modal-body {
      padding: 40px 50px;
  }
  .nav-tabs li a {
      color: #777;
  }
  #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
  }  
  .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
  }
  .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
  }
  .navbar-nav li a:hover {
      color: #fff !important;
  }
  .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
  }
  .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
  }
  .dropdown-menu li a {
      color: #000 !important;
  }
  .dropdown-menu li a:hover {
      background-color: red !important;
  }
  footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
  }
  footer a {
      color: #f5f5f5;
  }
  footer a:hover {
      color: #777;
      text-decoration: none;
  }  
  .form-control {
      border-radius: 0;
  }
  textarea {
      resize: none;
  }
  #mundial{
    background-image: url("mundial.jpeg")
  }
  </style>
</head>
<?php
include 'conx.php';   
?>
<body id="mundial" data-spy="scroll" data-target=".navbar" data-offset="50">
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Mundial</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="http://192.168.105.122/index.php">INICIO</a></li>
        <li><a href="http://192.168.105.122/tabla.php">RESULTADOS</a></li>
        <li><a href="http://192.168.105.122/selectGrupo.php">INTRODUCIR RESULTADOS</a></li>
      </ul>
    </div>
  </div>
</nav> 

<div class="container" style="background-color:white;">
<div class="row">
    <div class="col-xs-4">
      <span class="text-primary"><b> País </b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-primary"><b>Puntos</b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-primary"><b>Ganados</b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-primary"><b>Perdidos</b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-primary"><b>Empatados</b></span>
    </div>    
    <div class="col-xs-1">
      <span class="text-primary"><b>Jugados</b></span>
    </div>        
  </div>
<hr>

<?php
 include 'conx.php';
  $sql = '';
  if ( $_GET['g'] != '' ){
    $sql = 'select * from equipos where grupo = "'. $_GET['g'] . '" order by puntos desc;';    
  }
  else {
    $sql = 'select * from equipos order by grupo, puntos desc;';
  }
  $data = $c->query($sql);
  while ( $equipo = $data->fetch_assoc() ){
  $jugados = $equipo['ganados'] + $equipo['perdidos'] + $equipo['empatados'];
  ?>
  <div class="row">
    <div class="col-xs-4">
      <span class="text-success"><b><?= $equipo['pais'] ?></b></span>  
      (<?= $equipo['grupo'] ?>)
    </div>
    <div class="col-xs-1 text-left">
      <span class="text-primary"><b><?= $equipo['puntos'] ?></b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-success"><b><?= $equipo['ganados'] ?></b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-danger"><b><?= $equipo['perdidos'] ?></b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-warning"><b><?= $equipo['empatados'] ?></b></span>
    </div>
    <div class="col-xs-1">
      <span class="text-primary"><b><?= $jugados ?></b></span>
    </div>            
  </div>  

  <?php  
  }
  ?>
  <hr>
  
  <a href="http://192.168.105.122/prueba.php" class="btn center-block"> Introducir resultados </a>
  <a  class="btn center-block"data-toggle="collapse" href="#collapse1">Grupos</a>
  <div id="collapse1" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item"><a href="http://192.168.105.122/tabla.php?g=A">A</a></li>
        <li class="list-group-item"><a href="http://192.168.105.122/tabla.php?g=B">B</a></li>
        <li class="list-group-item"><a href="http://192.168.105.122/tabla.php?g=C">C</a></li>
        <li class="list-group-item"><a href="http://192.168.105.122/tabla.php">Todos</a></li>
      </ul>
    </div>
  
</body>
</html>
