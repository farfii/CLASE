// Se necesita tener instalado mediante el comando "npm" ExpressJS y MongoDB
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongo = require('mongodb')
const MongoClient = mongo.MongoClient;
const assert = require('assert');
app.use( bodyParser.json() );
// Le decimos que carpeta queremos que sea publica.
app.use( express.static('public') );
// En esta linea ponemos que los datos de conexion de la base de datos los pongamos al iniciar el servidor.
const url = process.argv[2];


// Acontinuación creamos una clase APP para poner ahi todas las conexiones con las tablas de la base de datos que pongamos al servidor.
class App{
    constructor(){}
    //Aqui se pondrian cada unas de las funciones que definamos y mas adelante las llamamos.
    //Solo ponemos publica es decir que se puede llamar desde cualquier direccion la de lista para que no puedan hacer mas que Leer la lista de datos. 
    //las demas solo para localhost.
    static dbs(req,res){
        MongoClient.connect(url, function(err, client) {
            const adminDb = client.db().admin();
            adminDb.listDatabases(function(err, dbs) {     
              res.send(dbs.databases)
            });    
          });
    }
    static lista(req,res){
        res.header("Access-Control-Allow-Origin", "*");
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('basededatos');
            client.db('basededatos').collection('coleccion').find().toArray((err, results) => res.send(results)) 
    })
    }
    static inserta(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            const db = client.db('basededatos');
            client.db('basededatos').collection('coleccion').insertOne(req.body)
    })
    }
    static borrar(req,res){
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            let id = req.body._id; delete req.body._id;
            const db = client.db('basededatos');
            client.db('basededatos').collection('coleccion').deleteOne( {_id: new mongo.ObjectID(id)},
            (err, data) => { 
            res.send(data)
        }) 
        })
    }
    static actualiza(req,res){
        MongoClient.connect(url, function(err, client) { 
            let id = req.body._id; delete req.body._id
            assert.equal(null, err);
            client.db('basededatos').collection('coleccion').update({_id: new mongo.ObjectId(id)}, req.body , function(err, res) {
                if (err) throw err;
                console.log("1 documento actualizado");
    
              })
          
    }) 
    }
}


//Aqui asignamos la direccion que deberia escuchar por ejemplo "http://localhost:3000/dbs" a esta direccion le manda los datos recibidos de la base de datos.
app.get('/dbs', App.dbs);
app.post('/inserta', App.inserta);
app.get('/lista', App.lista);
app.post('/actualizar', App.actualiza);
app.post('/borrar', App.borrar);





//Esto es para asignar los puertos por donde recibe las peticiones http.(Esto se puede cambiar)
app.listen(3000);