<?php

$conexion = new mysqli('192.168.105.179', 'root', 'a', 'test');
if ( $conexion->connect_error){ die('Error de conexiÃ³n'); }
$conexion->set_charset('utf8');

class Blog {
    function __construct($autor, $titulo, $categoria){
        $this->idblogs = 0;
        $this->autor = $autor;
        $this->titulo = $titulo;
        $this->fecha = date('Y-m-d');
        $this->categoria = $categoria;
        $this->contenido = '';

    }

    function insertaFila(){
        // inserta el objeto actual como nueva fila en la BD
        global $conexion;
        $sql = $conexion->prepare('insert into blogs values (0, ?, ?, ?, ?, ?) ');
        $sql->bind_param('sssss', $this->autor, $this->titulo, $this->fecha, $this->categoria, $this->contenido);
        $sql->execute();
    }

    function borraFila(){
        // borra el objeto actual de la BD
        global $conexion;
        $sql = $conexion->prepare('delete from blogs where idblogs = ? ');
        $sql->bind_param('i', $this->idblogs);
        $sql->execute();
    }

    function actualizaFila(){
        // actualiza el objeto actual en la BD
        global $conexion;
        $sql = $conexion->prepare('update blogs set autor = ?, titulo = ?, fecha = ?, categoria = ?, contenido = ? where idblogs = ? ');
        $sql->bind_param('sssssi', $this->autor, $this->titulo, $this->fecha, $this->categoria, $this->contenido, $this->idblogs);
        $sql->execute();
    }

    static function cogeFila($id){
        $obj = new Blog('','','');
        global $conexion;
        $sql = $conexion->prepare('select idblogs, autor, titulo, fecha, categoria, contenido from blogs where idblogs = ? ');
        $sql->bind_param('i', $id);
        $sql->execute();
        $sql->bind_result($obj->idblogs, $obj->autor, $obj->titulo, $obj->fecha, $obj->categoria, $obj->contenido );
        $sql->fetch();
        return $obj;
    }

    static function listaFilas(){        
        global $conexion ;       
        $datos = $conexion->query('select * from blogs');                    
        return $datos;
      } 

}


?>