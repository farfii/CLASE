function getPosts(){    
  $('#preview').on('click', n => { $('#contenido').toggle() })  
  $('#contenido').on('blur', n => {  $('#preview').html( $('#contenido').val() ) })
  $.getJSON('server.php', verLista).fail( m => { console.log('err')});  
}


function verLista(data){
  $('#lista').html('<h5> novedades </h5>')
  $('<ol/>', { id: 'lordenada'}).appendTo('#lista')  
  data.forEach( articulo => {
    $('<li/>', {
      html: `${articulo.titulo} (${articulo.autor})`,      
      'data-target': '#exampleModal',
      'data-toggle': 'modal',
      click: x => { verArticulo(articulo) }
    }).appendTo('#lordenada');
  });
  
}

function verArticulo(art){  
  $('#idblogs').val(art.idblogs)
  $('#autor').val(art.autor)
  $('#titulo').val(art.titulo)
  $('#contenido').val(art.contenido)
  $('#preview').html(`${art.contenido} ...`)
  $('#modalTitle').text(art.titulo)  
}

function guardar(){
  art = {
    idblogs: $('#idblogs').val(),
    autor: $('#autor').val(),
    titulo: $('#titulo').val(),
    contenido: $('#contenido').val(),
  }
  // y enviar con POST
  $.ajax({
    type: 'POST',
    url: 'server.php',  
    data: `idblogs=${art.idblogs}&autor=${art.autor}&titulo=${art.titulo}&contenido=${art.contenido}`,    
    success: d => { getPosts() }, 
    error: m => { }
  })
  
}