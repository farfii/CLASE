# !bin/bash/
# Monitorizacion en Zenity
discos(){
     disc=$(zenity --info \
     --text "`df -h`")
     case $disc in 
     *) menu1;;
     esac 
}
puertos(){
    puert=$(zenity --info \
    --text "`lsusb`")
    case $puert in
    *) menu1;;
    esac
}
usuarios(){
    usu=$(zenity --info \
    --text "`who | wc -l`")
    case $usu in
    *) menu1;;
    esac

}
servicios(){
    serv=$(zenity --info \
    --text "`sudo service --status-all | grep + `")
    case $serv in
    *) menu1 ;;
    esac
}
memoria(){
    parametro=$(zenity --entry \
--title="Selecciona el parametro" \
--text="Parametros(h , b , m , g) " \
--entry-text "Parametro")
completado1=$(zenity --info \
--text "` free -$parametro`")
case $completado1 in
      *) menu1;;
esac

}
sistema(){
    memoria=$(zenity --info\
    --text "`lsb_release -a`")
    case $memoria in 
    *) menu1;;
    esac 
}
red(){
 tarjeta=$(zenity --info\
 --text "`lspci | grep Wireless`")
 case $tarjeta in 
 *) menu1;;
 esac
}

ip1(){
    IP1=$(zenity --info\
    --text "` sudo ifconfig wlan0 | grep "inet" | cut -d: -f2 | cut -d " " -f1 `")
    case $IP1 in 
    *) menu1;;
    esac

}
ip2(){
    IP2=$(zenity --info \
    --text "` sudo ifconfig eth0 | grep "inet" | cut -d: -f2 | cut -d " " -f1 `")
    case $IP2 in 
    *) menu1;;
    esac
}
menu2(){
    zenity --question --width=250 --height=120 --title "Elige la opcion" \
    --ok-label="Si" --cancel-label="No" --text "¿Estas conectado por wifi?"
    case $? in 
    0) ip1 ;;
    1) ip2 ;;
    *) echo -e "Error.." ;;
    esac 
}
menu1(){
menu1=$(zenity --list --width=500 --height=300 \
--text="Elige lo que quieras ejecutar" --title="Monitorizacion con Zenit" --column="Selección" --column="Descripcion" \
Discos "Te muestra los discos activos" \
Usuarios "Numero de usuarios conectados" \
Puertos_USB "Puertos (si no hay puertos no te saldra)" \
Servicios "Servicios activos" \
Memoria_Libre "Te enseña la memoria libre con los parametros que escogas (B , MB o GB)" \
Sistema_Operativo "Te muestra que sistema operativo estas utilizando" \
Tarjeta_de_red "Te muestra tu tarjeta de red " \
IP "Muestra tu ip local y externa mas la mascara de subred" )


case $menu1 in
"Discos") discos;;
"Usuarios") usuarios;;
"Puertos_USB") puertos;;
"Servicios") servicios;;
"Memoria_Libre") memoria;;
"Sistema_Operativo") sistema;;
"Tarjeta_de_red") red;;
"IP") menu2;;
esac
}
zenity --question --width=250 --height=120 --title "Bienvenido al Programa de monitorizacion con Zenity " --ok-label="No" \
--cancel-label="Si" --text "Deseas abrir el programa de monitorizacion?" 
case $? in 
    0) exit 0;;
    1) menu1;;
    *) echo -e "Error.." ;;
    esac 