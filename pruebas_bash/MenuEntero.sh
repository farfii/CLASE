# !bin/bash/
# Funciones para el Zenity 
#Ponemos los comandos de instalacion de cada uno de los programas + la redireccion de la salida de los comandos a un archivo temporal.
#Funciones de instalación: instalacion chromium , instalacion visual code , instalacion LAMP , instalacion MEAN .
# reconfiguracion openssh.
sudo apt-get update 1<>/tmp/CONSOLE_LOG2
chrome(){
     sudo apt-get install chromium -y 1<>/tmp/CONSOLE_LOG2
     listo
}
VSC(){
     cd /tmp/
     sudo wget https://go.microsoft.com/fwlink/?LinkID=760868 1<>/tmp/CONSOLE_LOG2
     sudo mv index.html\?LinkID\=760868 code.deb 1<>/tmp/CONSOLE_LOG2
     sudo dpkg -i code.deb 1<>/tmp/CONSOLE_LOG2
     listo
}
LAMP(){
     sudo apt-get install apache2 -y  1<>/tmp/CONSOLE_LOG2
     echo "a" > /tmp/passwd.txt
     echo "a" >> /tmp/passwd.txt 
     sudo apt-get install mysql-server -y < /tmp/passwd.txt 
     sudo apt-get install mysql-workbench -y 1<>/tmp/CONSOLE_LOG2
     sudo apt-get install php5 -y 1<>/tmp/CONSOLE_LOG2
     listo
}
MEAN(){
     cd /tmp/
     wget https://nodejs.org/dist/v10.1.0/node-v10.1.0-linux-x64.tar.xz 1<>/tmp/CONSOLE_LOG2
     tar xf node-v10.1.0-linux-x64.tar.xz 1<>/tmp/CONSOLE_LOG2
     cd node-v10.1.0-linux-x64/ 1<>/tmp/CONSOLE_LOG2
     sudo rsync -av bin/ /usr/local/bin/ 1<>/tmp/CONSOLE_LOG2
     sudo rsync -av lib/ /usr/local/lib/ 1<>/tmp/CONSOLE_LOG2
     sudo npm install npm@latest -g 1<>/tmp/CONSOLE_LOG2
     sudo npm i nodemon -g 1<>/tmp/CONSOLE_LOG2
     npm install expressjs 1<>/tmp/CONSOLE_LOG2
     sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5 1<>/tmp/CONSOLE_LOG2
     echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.6 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list 1<>/tmp/CONSOLE_LOG2
     sudo apt-get update 1<>/tmp/CONSOLE_LOG2
     sudo apt-get install -y mongodb-org 1<>/tmp/CONSOLE_LOG2
     listo
}
openssh(){
     sudo apt-get remove openssh-server --purge -y 1<>/tmp/CONSOLE_LOG2
     sudo apt-get install openssh-server -y 1<>/tmp/CONSOLE_LOG2
     listo
}
#Menu de instalacion Zenity
# Le doy nombre a la eleccion del usuario para poder ponerla en un case y asi ejectuar diferentes comandos 
menu_zenity(){
   seleccion=$(zenity --list --width=500 --height=300 --cancel-label="Volver" \
     --text="Elige el programa que deseas instalar" --title="Instalador" --column="APP" --column="Descripcion"\
    Chromium "Alternativa al Google Chrome(Navegador Web)."\
    Visual_Studio_Code "Programa para editar codigos de programacion.Sirve para la mayoria de codigos."\
    LAMP "GNU/Linux , Apache2/HTTP SERVER , MySQL y PHP"\
    MEAN "MongoDB , ExpressJS , AngularJS , NodeJS"\
    Openssh_Server "Conjunto de aplicaciones que permiten realizar comunicaciones cifradas a través de una red, usando el protocolo SSH."\
    Salir "")
    case $seleccion in
    "Volver") inicio;; 
    "Chromium") chrome ;;
    "Visual_Studio_Code") VSC ;;
    "LAMP") LAMP ;;
    "MEAN") MEAN ;;
    "Openssh_Server") openssh ;;
    "Salir") salida;;
    esac
}
#Ventana de comprobacion 
listo(){
    completado=$(zenity --info \
    --text="Instalacion completada!")
    case $completado in
    *) menu_zenity;;
    esac
}
#Funciones de Monitorizacion : discos , puertos , memoria , usuarios , servicios , sistema operativo ,
# red , ip. + opcion entre conexion con cable y wifi .
discos(){
     disc=$(zenity --info \
     --text "`df -h`")
     case $disc in 
     *) menu1;;
     esac 
}
puertos(){
    puert=$(zenity --info \
    --text "`lsusb`")
    case $puert in
    *) menu1;;
    esac
}
usuarios(){
    usu=$(zenity --info \
    --text "`who | wc -l`")
    case $usu in
    *) menu1;;
    esac

}
servicios(){
    serv=$(zenity --info \
    --text "`sudo service --status-all | grep + `")
    case $serv in
    *) menu1 ;;
    esac
}
memoria(){
    parametro=$(zenity --entry \
--title="Selecciona el parametro" \
--text="Parametros(h , b , m , g) " \
--entry-text "Parametro")
completado1=$(zenity --info \
--text "` free -$parametro`")
case $completado1 in
      *) menu1;;
esac

}
sistema(){
    memoria=$(zenity --info\
    --text "`lsb_release -a`")
    case $memoria in 
    *) menu1;;
    esac 
}
red(){
 tarjeta=$(zenity --info\
 --text "`lspci | grep Wireless`")
 case $tarjeta in 
 *) menu1;;
 esac
}

ip1(){
    IP1=$(zenity --info\
    --text "` sudo ifconfig wlan0 | grep "inet" | cut -d: -f2 | cut -d " " -f1 `")
    case $IP1 in 
    *) menu1;;
    esac

}
ip2(){
    IP2=$(zenity --info \
    --text "` sudo ifconfig eth0 | grep "inet" | cut -d: -f2 | cut -d " " -f1 `")
    case $IP2 in 
    *) menu1;;
    esac
}
#Menu para la opcion entre cable y wifi 
menu2(){
    zenity --question --width=250 --height=120 --title "Elige la opcion" \
    --ok-label="Si" --cancel-label="No" --text "¿Estas conectado por wifi?"
    case $? in 
    0) ip1 ;;
    1) ip2 ;;
    *) echo -e "Error fatal.." ;;
    esac 
}
#Menu de monitorizacion
menu1(){
menu1=$(zenity --list --width=500 --height=300 --cancel-label="Volver" \
--text="Elige lo que quieras ejecutar" --title="Monitorizacion con Zenit" --column="Selección" --column="Descripcion" \
Discos "Te muestra los discos activos" \
Usuarios "Numero de usuarios conectados" \
Puertos_USB "Puertos (si no hay puertos no te saldra)" \
Servicios "Servicios activos" \
Memoria_Libre "Te enseña la memoria libre con los parametros que escogas (B , MB o GB)" \
Sistema_Operativo "Te muestra que sistema operativo estas utilizando" \
Tarjeta_de_red "Te muestra tu tarjeta de red " \
IP "Muestra tu ip local y externa mas la mascara de subred" \
Salir ""  )


case $menu1 in
"Volver") inicio;;
"Discos") discos;;
"Usuarios") usuarios;;
"Puertos_USB") puertos;;
"Servicios") servicios;;
"Memoria_Libre") memoria;;
"Sistema_Operativo") sistema;;
"Tarjeta_de_red") red;;
"IP") menu2;;
"Salir") salida;;
esac
}
#Menu para salir del programa
salida(){
    zenity --question --width=250 --height=120 --title "Programa Zenity" --ok-label="Salir" \
    --cancel-label="Volver" --text "¿Deseas salir del programa?"
    case $? in
    0) exit 0;;
    1) inicio;;
    *) echo -e "Error fatal.."
    esac 
}
# Ventana inicial del programa
inicio(){
zenity --question --width=250 --height=120 --title "Bienvenido al Programa Zenity v3" --ok-label="menu de monitorizacion" \
--cancel-label="menu de instalacion" --text "Que menu deseas iniciar?" 
case $? in 
    0) menu1;;
    1) menu_zenity;;
    *) echo -e "Error fatal.."
    esac 
}
#Hacemos un bucle para que no se cierre el programa 
inicio
while true
do  
inicio 
done
