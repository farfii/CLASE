# ! bin/bash/
#PROGRAMA PARA INSTALAR LAMP Y MEAN 
pause(){
  read -p "Pulsa [INTRO] para continuar..." fackEnterKey
}
# Dentro de 1 estan todos los comandos necesarios para instalar MEAN()
one(){
    sudo apt-get install apache2 -y 
    sudo apt-get install mysql-server -y a a 
    sudo apt-get install mysql-workbench
	echo "Listo!"
        pause
}
 
# Dentro de 2 estan todos los comandos necesarios para instalar LAMP()
two(){
    npm install expressjs
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
    echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.6 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org 
	echo "Listo!"
        pause
}
 
# funcion para desplegar el menu
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo "  Menú de instalación   "
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Instalar MEAN"
	echo "2. Instalar LAMP"
	echo "3. Salir"
}
# Lee la accion sobre el teclado y la ejecuta.
# Invoca el () cuando el usuario selecciona 1 en el menú.
# Invoca a los dos () cuando el usuario selecciona 2 en el menú.
# Salir del menu cuando el usuario selecciona 3 en el menú.
read_options(){
	local choice
	read -p "Elige una opción [ 1 - 3] " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) exit 0;;
		*) echo -e "${RED}Lo siento ese numero no es una opcion..${STD}" && sleep 2
	esac
}
 
# ----------------------------------------------
# No permite usar CTRL+C ni CTRL+z
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Para que se ejecute infinitamente hasta que el usario le de a la opción de salir.
# ------------------------------------
while true
do
 
	show_menus
	read_options
done