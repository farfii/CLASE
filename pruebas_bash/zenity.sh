# !bin/bash/
# Funciones para el Zenity 
#Ponemos los comandos de instalacion de cada uno de los programas + la redireccion de la salida de los comandos a un archivo temporal.
sudo apt-get update 1<>/tmp/CONSOLE_LOG2
chrome(){
     sudo apt-get install chromium -y 1<>/tmp/CONSOLE_LOG2
     listo
}
VSC(){
     cd /tmp/
     sudo wget https://go.microsoft.com/fwlink/?LinkID=760868 1<>/tmp/CONSOLE_LOG2
     sudo mv index.html\?LinkID\=760868 code.deb 1<>/tmp/CONSOLE_LOG2
     sudo dpkg -i code.deb 1<>/tmp/CONSOLE_LOG2
     listo
}
LAMP(){
     sudo apt-get install apache2 -y  1<>/tmp/CONSOLE_LOG2
     echo "a" > /tmp/passwd.txt
     echo "a" >> /tmp/passwd.txt 
     sudo apt-get install mysql-server -y < /tmp/passwd.txt 
     sudo apt-get install mysql-workbench -y 1<>/tmp/CONSOLE_LOG2
     listo
}
MEAN(){
     cd /tmp/
     wget https://nodejs.org/dist/v10.1.0/node-v10.1.0-linux-x64.tar.xz 1<>/tmp/CONSOLE_LOG2
     tar xf node-v10.1.0-linux-x64.tar.xz 1<>/tmp/CONSOLE_LOG2
     cd node-v10.1.0-linux-x64/ 1<>/tmp/CONSOLE_LOG2
     sudo rsync -av bin/ /usr/local/bin/ 1<>/tmp/CONSOLE_LOG2
     sudo rsync -av lib/ /usr/local/lib/ 1<>/tmp/CONSOLE_LOG2
     sudo npm install npm@latest -g 1<>/tmp/CONSOLE_LOG2
     sudo npm i nodemon -g 1<>/tmp/CONSOLE_LOG2
     npm install expressjs 1<>/tmp/CONSOLE_LOG2
     sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5 1<>/tmp/CONSOLE_LOG2
     echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.6 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list 1<>/tmp/CONSOLE_LOG2
     sudo apt-get update 1<>/tmp/CONSOLE_LOG2
     sudo apt-get install -y mongodb-org 1<>/tmp/CONSOLE_LOG2
     listo
}
openssh(){
     sudo apt-get remove openssh-server --purge -y 1<>/tmp/CONSOLE_LOG2
     sudo apt-get install openssh-server -y 1<>/tmp/CONSOLE_LOG2
     listo
}
#Menu de instalacion Zenity
# Le doy nombre a la eleccion del usuario para poder ponerla en un case y asi ejectuar diferentes comandos 
menu_zenity(){
   seleccion=$(zenity --list --width=500 --height=300\
     --text="Elige el programa que deseas instalar" --title="Instalador" --column="APP" --column="Descripcion"\
    Chromium "Alternativa al Google Chrome(Navegador Web)."\
    Visual_Studio_Code "Programa para editar codigos de programacion.Sirve para la mayoria de codigos."\
    LAMP "GNU/Linux , Apache2/HTTP SERVER , MySQL y PHP"\
    MEAN "MongoDB , ExpressJS , AngularJS , NodeJS"\
    Openssh_Server "Conjunto de aplicaciones que permiten realizar comunicaciones cifradas a través de una red, usando el protocolo SSH.") 
    case $seleccion in 
    "Chromium") chrome ;;
    "Visual_Studio_Code") VSC ;;
    "LAMP") LAMP ;;
    "MEAN") MEAN ;;
    "Openssh_Server") openssh ;;
    esac
}
#Ventana de comprobacion 

listo(){
    completado=$(zenity --info \
    --text="Instalacion completada!")
    case $completado in
    *) menu_zenity;;
    esac
}
# Ventana inicial del programa
zenity --question --width=250 --height=120 --title "Bienvenido al Programa Zenity v1.0" --ok-label="No" \
--cancel-label="Si" --text "Desea iniciar el menu de instalacion definitivo?" 
case $? in 
    0) exit 0;;
    1) menu_zenity;;
    *) echo -e "Error.." ;;
    esac 
    